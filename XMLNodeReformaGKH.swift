//
//  XMLNodeReformaGKH.swift
//  Flatmate
//
//  Created by Aleksey Larichev on 29/12/2018.
//  Copyright © 2018 Aleksey Larichev. All rights reserved.
//

import UIKit
class XMLNodeReformaGKH: NSObject {
    
    var name: String
    var content: String?
    weak var parent:XMLNodeReformaGKH?
    
    fileprivate (set) var children:[XMLNodeReformaGKH]?
    
    init(name:String) {
        self.name = name
        super.init()
    }

    func addChild(_ node:XMLNodeReformaGKH) {
        if children == nil { children = [XMLNodeReformaGKH]() }
        children?.append(node)
        node.parent = self
    }

    func dictionaryRepresentation() -> AnyObject {
        guard children?.count > 0 else {
            return content as AnyObject? ?? "" as AnyObject
        }
        var childrenByName = [String : [AnyObject]]()
        
        // children здесь больше нуля!
        for child in children! {
            let childRepresentation = child.dictionaryRepresentation()
             if var existingArray = childrenByName[child.name]{
                existingArray.append(childRepresentation)
                childrenByName[child.name] = existingArray
            } else {
                childrenByName[child.name] = [childRepresentation]
            }
        }
        
        var dictionary = [String : AnyObject]()
        for (name, array) in childrenByName {
             if array.count == 1 {
                if (array[0] as? String) != "" {
                    dictionary[name] = array[0]
                }
            } else {
                dictionary[name] = array as AnyObject?
            }
        }
        return dictionary as AnyObject
    }
}

private func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

private func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}
