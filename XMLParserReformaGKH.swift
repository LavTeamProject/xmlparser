//
//  XMLParserReformaGKH.swift
//  Flatmate
//
//  Created by Aleksey Larichev on 29/12/2018.
//  Copyright © 2018 Aleksey Larichev. All rights reserved.
//

import UIKit


/// Example use: XMLParserReformaGKH().getJsonData(xmldata: xmlData) }.
class XMLParserReformaGKH: NSObject {

    fileprivate var root:XMLNodeReformaGKH!
    fileprivate var currentNode:XMLNodeReformaGKH!
    fileprivate var characters:String!
    fileprivate var parser: XMLParser!

    var removeNamespaces:Bool = false
    var jsonData: Data?

    override init() { }

    func getJsonData(xmldata:Data) -> Data? {
        //        print("XML String:\n\n "String(data: xmldata, encoding: String.Encoding.utf8) as String!)
        parser = XMLParser(data: xmldata)
        self.parser.delegate = self
        start()
        return jsonData
    }
    
    func start() {
        if removeNamespaces {
            parser.shouldProcessNamespaces = true
            parser.shouldReportNamespacePrefixes = false
        }
        parser.parse()
        let dict = self.root.dictionaryRepresentation() as? [String : Any]
        
        let jsonString = dict.map{ dictionaryToJsonString(dict: $0) }??
            .replacingOccurrences(of: "SOAP-ENV:", with: "")
            .replacingOccurrences(of: "ns1:", with: "") ?? ""
        
        print(jsonString as NSString)
        
        self.jsonData = jsonString.data(using: .utf8)
    }
}

// MARK: - XMLParserDelegate
extension XMLParserReformaGKH: XMLParserDelegate {
    func parserDidStartDocument(_ parser: XMLParser) {
        root = XMLNodeReformaGKH(name: "xml")
        currentNode = root
    }
    func parserDidEndDocument(_ parser: XMLParser) { }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) { }
    
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String]) {
        
        characters = nil
        let newNode = XMLNodeReformaGKH(name: elementName)
        currentNode.addChild(newNode)
        currentNode = newNode
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if characters == nil { characters = "" }
        characters.append(string)
        print("value: ", string)
    }
    
    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?) {
        if characters != nil {
            currentNode.content = characters
            characters = nil
        }
        currentNode = currentNode.parent
    }
    
    private func dictionaryToJsonString(dict: [String:Any]) -> String? {
        if let theJSONData = try? JSONSerialization.data( withJSONObject: dict, options: []) {
            let theJSONText = String(data: theJSONData, encoding: .utf8)
            return theJSONText
        } else {
            return nil
        }
    }
}
